package capra.mihai.todolist.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import capra.mihai.todolist.R
import capra.mihai.todolist.models.Task
import capra.mihai.todolist.todo.ToDoList
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import kotlinx.android.synthetic.main.task_item.view.*

class TasksAdapter(
    val options: FirebaseRecyclerOptions<Task>,
    private val clickListener: (Task) -> Unit,
    private val deleteListener: (Task) -> Unit,
    private val editListener: (Task) -> Unit,
    private val completeListener: (Task) -> Unit
) : FirebaseRecyclerAdapter<Task, RecyclerView.ViewHolder>(options) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TaskViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.task_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, model: Task) {
        (holder as TaskViewHolder).bind(
            model,
            clickListener,
            deleteListener,
            editListener,
            completeListener
        )
    }

    class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            task: Task,
            listener: (Task) -> Unit,
            deleteListener: (Task) -> Unit,
            editListener: (Task) -> Unit,
            completeListener: (Task) -> Unit
        ) = with(itemView) {
            if (task.isCompleted) {
                itemView.foregroundView.setBackgroundColor(
                    ContextCompat.getColor(
                        ToDoList.toDoInstance,
                        R.color.comleted_task_color
                    )
                )
            }
            taskTitle.text = task.title
            taskDescription.text = task.description
            taskDeleteBtn.setOnClickListener { deleteListener(task) }
            taskEditBtn.setOnClickListener { editListener(task) }
            completeButton.setOnClickListener { completeListener(task) }
            setOnClickListener { listener(task) }
        }
    }
}