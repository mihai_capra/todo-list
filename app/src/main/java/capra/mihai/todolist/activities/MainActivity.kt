package capra.mihai.todolist.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import capra.mihai.todolist.R
import capra.mihai.todolist.activities.base.BaseActivity
import capra.mihai.todolist.adapters.TasksAdapter
import capra.mihai.todolist.models.Task
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.create_new_task_popup.view.*
import java.util.*

class MainActivity : BaseActivity() {
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private var mTaskAdapter: TasksAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDatabase = FirebaseDatabase.getInstance().reference

        fabNewTask.setOnClickListener {
            showCreateNewTaskPopup()
        }

        mLinearLayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        mLinearLayoutManager.reverseLayout = true
        mLinearLayoutManager.stackFromEnd = true
        taskRecycler.setHasFixedSize(true)
        taskRecycler.layoutManager = mLinearLayoutManager

        val tasksQuery = getAllTasks()

        val options = FirebaseRecyclerOptions.Builder<Task>()
            .setQuery(tasksQuery, Task::class.java)
            .setLifecycleOwner(this)
            .build()
        println("OPTIONS: $options")
        println("QUERY: $tasksQuery")
        mTaskAdapter = TasksAdapter(
            options,
            { task: Task -> onTaskClicked(task) },
            { task: Task -> onTaskDelete(task) },
            { task: Task -> onTaskEdit(task) },
            { task: Task -> onTaskComplete(task) })

        taskRecycler.adapter = mTaskAdapter

    }

    override fun onStart() {
        super.onStart()
        if (mTaskAdapter != null) {
            mTaskAdapter!!.startListening()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mTaskAdapter != null) {
            mTaskAdapter!!.stopListening()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_logout -> {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun writeNewTask(userId: String, title: String, description: String) {
        val id = mDatabase.child("/tasks/").push().key
        val post = Task(id!!, userId, title, description)
        val postValues = post.toMap()

        val childUpdates = HashMap<String, Any>()
        childUpdates["/tasks/$id"] = postValues
        if (mTaskAdapter != null) mTaskAdapter!!.notifyDataSetChanged()
        mDatabase.updateChildren(childUpdates)
    }

    private fun editTask(task: Task, title: String, description: String) {
        val dRef = mDatabase.child("/tasks/").child(task.userId)
        task.title = title
        task.description = description
        dRef.setValue(task)
        if (mTaskAdapter != null) mTaskAdapter!!.notifyDataSetChanged()
    }

    private fun setCompleteTask(task: Task) {
        val dRef = mDatabase.child("/tasks/").child(task.userId)
        task.isCompleted = true
        dRef.setValue(task)
        if (mTaskAdapter != null) mTaskAdapter!!.notifyDataSetChanged()
    }

    private fun showCreateNewTaskPopup(task: Task? = null) {
        val inflater = LayoutInflater.from(this@MainActivity)
        val createTaskLayout = inflater.inflate(R.layout.create_new_task_popup, null)
        val alertBuilder = AlertDialog.Builder(this@MainActivity)

        val alertDialog = alertBuilder.create()
        alertDialog.setView(createTaskLayout)
        alertDialog.setCancelable(true)
        alertDialog.create()

        if (task != null) {
            createTaskLayout.titleInput.setText(task.title)
            createTaskLayout.descriptionInput.setText(task.description)
        }

        createTaskLayout.createNewPostBtn.setOnClickListener {
            if (createTaskLayout.titleInput.text.isNotEmpty() && createTaskLayout.descriptionInput.text.isNotEmpty()) {
                alertDialog.dismiss()
                if (task != null) {
                    editTask(
                        task,
                        createTaskLayout.titleInput.text.toString(),
                        createTaskLayout.descriptionInput.text.toString()
                    )
                } else {
                    writeNewTask(
                        getUid(),
                        createTaskLayout.titleInput.text.toString(),
                        createTaskLayout.descriptionInput.text.toString()
                    )
                }
            } else {
                validateInputs(createTaskLayout.titleInput, createTaskLayout.descriptionInput)
            }
        }

        alertDialog.show()
    }

    private fun validateInputs(titleInput: EditText, descriptionInput: EditText) {
        if (TextUtils.isEmpty(titleInput.text.toString())) {
            titleInput.error = "Required"
        } else {
            titleInput.error = null
        }

        if (TextUtils.isEmpty(descriptionInput.text.toString())) {
            descriptionInput.error = "Required"
        } else {
            descriptionInput.error = null
        }
    }

    private fun getUid(): String {
        return FirebaseAuth.getInstance().currentUser!!.uid
    }

    private fun getAllTasks(): Query {
        return mDatabase.child("/tasks/")
    }

    private fun onTaskClicked(task: Task) {

    }

    private fun onTaskDelete(task: Task) {
        mDatabase.child("/tasks/").child(task.userId).removeValue()
    }

    private fun onTaskEdit(task: Task) {
        if (!task.isCompleted) {
            showCreateNewTaskPopup(task)
        }
    }

    private fun onTaskComplete(task: Task) {
        if (!task.isCompleted) {
            setCompleteTask(task)
        }
    }
}
