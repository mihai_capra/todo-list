package capra.mihai.todolist.activities.base

import android.support.v7.app.AppCompatActivity
import com.kaopiz.kprogresshud.KProgressHUD

open class BaseActivity : AppCompatActivity() {

    private var mHud: KProgressHUD? = null

    fun showProgressHud() {
        if (mHud == null) {
            mHud = KProgressHUD(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setAnimationSpeed(3)
                .setDimAmount(0.5f)
                .setCancellable(false)
                .show()
        }
    }

    fun hideProgressHud() {
        if (mHud != null && mHud!!.isShowing) {
            mHud!!.dismiss()
        }
    }
}