package capra.mihai.todolist.activities

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import capra.mihai.todolist.R
import capra.mihai.todolist.activities.base.BaseActivity
import capra.mihai.todolist.models.User
import capra.mihai.todolist.utils.Utils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.feedback_snackbar.*

class LoginActivity : BaseActivity(), View.OnClickListener {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var mDatabase: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance().reference

        loginButton.setOnClickListener(this)
        registerButton.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (mAuth.currentUser != null) {
            onAuthSuccess(mAuth.currentUser!!)
        }
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.registerButton -> {
                signUp()
            }
            R.id.loginButton -> {
                signIn()
            }
        }
    }

    private fun onAuthSuccess(user: FirebaseUser) {
        val username = usernameFromEmail(user.email!!)
        writeNewUser(user.uid, username, user.email!!)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun usernameFromEmail(email: String): String {
        return if (email.contains("@")) {
            email.split("@".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
        } else {
            email
        }
    }

    private fun validateInput(): Boolean {
        var result = true
        if (TextUtils.isEmpty(emailInput.text.toString())) {
            emailInput.error = "Required"
            result = false
        } else {
            emailInput.error = null
        }

        if (TextUtils.isEmpty(passwordInput.text.toString())) {
            passwordInput.error = "Required"
            result = false
        } else {
            passwordInput.error = null
        }

        return result
    }

    private fun writeNewUser(userId: String, name: String, email: String) {
        val user = User(name, email)
        mDatabase.child("users").child(userId).setValue(user)
    }

    private fun signIn() {
        if (!validateInput()) {
            return
        }
        showProgressHud()
        val email = emailInput.text.toString()
        val password = passwordInput.text.toString()
        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    hideProgressHud()
                    Utils.feedbackSnackBar(
                        feedbackSnackbar,
                        "You have successfully logged in."
                    ) {
                        onAuthSuccess(task.result!!.user)
                    }
                } else {
                    hideProgressHud()
                    Utils.feedbackSnackBar(
                        feedbackSnackbar,
                        "Login failed!"
                    ) {}
                }
            }
    }

    private fun signUp() {
        if (!validateInput()) {
            return
        }

        showProgressHud()

        val email = emailInput.text.toString()
        val password = passwordInput.text.toString()

        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                hideProgressHud()
                if (task.isSuccessful) {
                    Utils.feedbackSnackBar(
                        feedbackSnackbar,
                        "You have been successfully registered."
                    ) {
                        onAuthSuccess(task.result!!.user)
                    }
                } else {
                    hideProgressHud()
                    Utils.feedbackSnackBar(
                        feedbackSnackbar,
                        "Register failed!"
                    ) {}
                }
            }
    }
}
