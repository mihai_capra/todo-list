package capra.mihai.todolist.utils

import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.util.Patterns

object Utils {
    fun isEmailValid(email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    fun feedbackSnackBar(
        layout: CoordinatorLayout,
        messageValue: String,
        callbackMethod: () -> Unit
    ) {
        Snackbar.make(
            layout,
            messageValue,
            Snackbar.LENGTH_SHORT
        ).addCallback(object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                callbackMethod()
            }
        }).show()
    }
}