package capra.mihai.todolist.models

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Task(
    val userId: String,
    val uid: String,
    var title: String,
    var description: String,
    var isCompleted: Boolean = false
) {
    constructor() : this("", "", "", "")

    @Exclude
    fun toMap(): Map<String, Any> {
        val result = HashMap<String, Any>()
        result["userId"] = userId
        result["uid"] = uid
        result["title"] = title
        result["description"] = description
        result["isCompleted"] = isCompleted
        return result
    }
}