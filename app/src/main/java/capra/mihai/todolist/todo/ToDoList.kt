package capra.mihai.todolist.todo

import android.app.Application
import com.google.firebase.database.FirebaseDatabase

class ToDoList : Application() {
    companion object {
        lateinit var toDoInstance: ToDoList
            private set
    }

    override fun onCreate() {
        super.onCreate()
        toDoInstance = this
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
    }
}